FROM node 

RUN mkdir /skill
WORKDIR /skill
COPY package.json /skill
RUN yarn install

COPY . /skill

RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
